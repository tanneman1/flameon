# FlameON
Simple FHIR client written in Vue3. Accessible on https://flameon.netlify.app/.

## How to get started?

### Install npm deps
```
npm install
```

### run the build
```
npm run build
```

### Run in dev mode

```
npm run dev
```